class CurrencyConverter extends React.Component {  
  constructor(props) {
    super(props);
    this.state = {amount: this.convertCurrency(props.initialPrice)};
  }

  handleChange(e) {
  	let amount = e.target.value;
  	let formattedAmount = this.convertCurrency(amount);
  	this.setState({amount: formattedAmount});
  }

  convertCurrency(amount) {
  	fx.base = "AUD";
  	fx.rates = {"USD": 0.73};
  	let convertedAmount = fx.convert(amount, {from: "AUD", to: "USD"});
  	return currency(convertedAmount).format();
  }

  handleSubmit(e) {
  	e.preventDefault();
  }

  render() {
    return (
    	<div>
    		<h1>Currency Converter</h1>
    		<form onSubmit={this.handleSubmit.bind(this)}>
	    		<label>AUD</label>
	    		<input type="text" onChange={this.handleChange.bind(this)} intialValue={this.state.amount} />
	    	</form>
	    	<div>
	    		USD: $<span>{this.state.amount}</span>
	    	</div>
    	</div>
    	
   	);
  }
}

CurrencyConverter.propTypes = { amount: React.PropTypes.number };
CurrencyConverter.defaultProps = { initialPrice: 0 };