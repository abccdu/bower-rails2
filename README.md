# README #

this was a test using the bower-rails gem instead of rails-assets.org as rails-assets is a service which might be going away soon...

https://github.com/rharriso/bower-rails/

The main take aways are:

this creates a Bowerfile file which is equivalent to a Gemfile file but for Bower projects

You call 
```
#!ruby

rake bower:install
```
 to install the bower files instead of 
```
#!ruby

bundle install
```


it's a little inconsistent on how you call in the assets (for the materialize library I just had to call //= require materialize but for the currency library I had to call //= require currency.js/currency)

It puts the assets into /vendor/assets/bower_components